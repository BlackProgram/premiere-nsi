from faker import Faker

fake = Faker('fr_FR')

with open('noms_adresses.csv', 'w', newline='\r\n') as f:
    f.write(fake.csv(header=('Adresse', 'Nom'),
               data_columns=('{{address}}', '{{name}}'), num_rows=21))

