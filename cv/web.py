from flask import Flask, escape, request, render_template

app =  Flask(__name__)
""" index contient les variables name, firstname et birth, variables qui prennent des valeurs différentes en fonctionn de ce qui est écrit dans l'url du CV..
"""

@app.route('/')
def index(): #fonction
        name = request.args.get('name') #variable pour le nom
        firstname = request.args.get('firstname') #variable pour le prénom
        birth = int(request.args.get('birth')) #variable pour l'année de naissance

        age = 2019-birth #donne l'âge par soustraction

        return render_template('cv.html', nom=name, prenom=firstname, annee=age)
        #affiche les infos sur la page web
