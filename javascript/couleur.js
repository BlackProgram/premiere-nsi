function couleur() {
    let rinput = document.getElementById("r") ;
    let r = rinput.value ;
    let vinput = document.getElementById("v") ;
    let v = vinput.value ;
    let binput = document.getElementById("b") ;
    let b = binput.value ;

    let couleur = `rgb(${r},${v},${b})` ;
    let div = document.getElementById("couleur") ;
    div.style.background = couleur ;
}
let b = document.getElementById("bouton") ;
b.addEventListener('click', couleur) ;
